The time complexity analysis of Huffman Coding is as follows:-

-extractMin( ) is called '2x(n-1)' times if there are 'n' nodes.
-As extractMin( ) calls minHeapify( ), it takes 'O(logn)' time.
 

Thus, Overall time complexity of Huffman Coding becomes : O(nlogn).

Here, 'n' is the number of unique characters in the given text.
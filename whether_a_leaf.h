int isLeaf(struct MinHeapNode* root) 
{ 
     return !(root->left) && !(root->right); 
} 
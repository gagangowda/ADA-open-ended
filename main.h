#include <stdio.h> 
#include <stdlib.h> 
#include<algorithm.h>
#include<struct_dec.h>
#include<create_minheap_structure.h>
#include<swap.h>
#include<min_heapify.h>
#include<extract_minimum_structure.h>
#include<insert_n_build_minheap.h>
#include<print_array.h>
#include<whether_a_leaf.h>
#include<create_n_build_minheap.h>
#include<huffman_tree.h>
#include<print_code.h>
#include<huffman_function.h>
#include<timecomp.h>
#define MAX_TREE_HT 100 

int main() 
{ 
  
    char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' }; 
    int freq[] = { 5, 9, 12, 13, 16, 45 }; 
  
    int size = sizeof(arr) / sizeof(arr[0]); 
  
    HuffmanCodes(arr, freq, size); 
  
    return 0; 
} 
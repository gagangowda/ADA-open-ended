void printCodes(struct MinHeapNode* root, int arr[], int top) 
  
{ 
  
    // Assign 0 to left edge and recur 
    if (root->left)
    { 
  
        arr[top] = 0; 
        printCodes(root->left, arr, top + 1); 
    } 
  
    // Assign 1 to right edge and recur 
    if (root->right)
    { 
  
        arr[top] = 1; 
        printCodes(root->right, arr, top + 1); 
    } 
   
    if (isLeaf(root)) 
    { 
  
        printf("%c: ", root->data); 
        printArr(arr, top); 
    } 
} 
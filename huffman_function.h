void HuffmanCodes(char data[], int freq[], int size) 
  
{ 
    // Construct Huffman Tree 
    struct MinHeapNode* root = buildHuffmanTree(data, freq, size); 
  
    int arr[MAX_TREE_HT], top = 0; 
  
    printCodes(root, arr, top); 
} 
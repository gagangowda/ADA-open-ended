void swapMinHeapNode(struct MinHeapNode** a,struct MinHeapNode** b) 
{   
    struct MinHeapNode* t = *a; 
    *a = *b; 
    *b = t; 
} 
int isSizeOne(struct MinHeap* minHeap) 
{ 
  
    return (minHeap->size == 1); 
} 
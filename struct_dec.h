struct MinHeapNode 
{     
    char data; 
    unsigned freq; 
    struct MinHeapNode *left, *right; 
};  
struct MinHeap 
{ 
  
    
    unsigned size; 
    unsigned capacity; 
    struct MinHeapNode** array; 
}; 